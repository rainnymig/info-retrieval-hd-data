import numpy as np
arr1 = [1, 2, 2, 3, 3, 4, 4, 4, 5, 6, 7, 8, 8]
arr2 = [1, 2, 3, 4, 4, 7, 8, 8]
arr3 = [3, 1, 5, 6, 2, 2, 1, 0, 8, 10]


nparr1 = np.array(arr1)
nparr2 = np.array(arr2)
nparr3 = np.array(arr3)

print(np.concatenate((nparr1, nparr2)))

print(np.argsort(nparr3))
