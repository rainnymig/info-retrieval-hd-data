#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 19:46:22 2017

@author: rainnymig
"""

import numpy as np
from scipy import misc
import matplotlib.pyplot as plt

N = 1000
start = 1200

pathPrefix = './mnist/'
pathDirs = [1, 4, 8]
imgIds = range(start, start + N)

X = []    # vectorized images
C = []    # RGB labels to identify X

for pathDir in pathDirs:
    for idx in imgIds:
        X.append(misc.imread(pathPrefix + 'd' + str(pathDir) + '/d' +
                             str(pathDir) + '_' + str(idx) +
                             '.png').ravel() / 255)
        if pathDir == pathDirs[0]:
            C.append([0, 0, 1])
        elif pathDir == pathDirs[1]:
            C.append([0, 1, 0])
        elif pathDir == pathDirs[2]:
            C.append([1, 0, 0])


X = np.array(X).T
C = np.array(C)

mu = np.average(X, 1)
X_c = X - np.expand_dims(mu, 1)

[U, Sigma, VT] = np.linalg.svd(X_c, full_matrices=False)

mu_rs = np.reshape(mu, (28, 28))
U_piece1 = np.reshape(U[:, 0], (28, 28))
U_piece2 = np.reshape(U[:, 1], (28, 28))
U_piece3 = np.reshape(U[:, 2], (28, 28))

plt.subplot(2, 2, 1)
plt.imshow(mu_rs, cmap="gray")
plt.subplot(2, 2, 2)
plt.imshow(U_piece1, cmap="gray")
plt.subplot(2, 2, 3)
plt.imshow(U_piece2, cmap="gray")
plt.subplot(2, 2, 4)
plt.imshow(U_piece3, cmap="gray")

S = np.dot(np.diag(Sigma), VT)
plt.figure()
plt.scatter(S[0], S[1], c = C)
plt.show()