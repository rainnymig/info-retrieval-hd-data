#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 21:56:49 2017

@author: rainnymig
"""

import numpy as np

def toyrnd(n):
    if not isinstance(n, int):
        return "Not gonna do that. You need an int"
    elif n <= 0:
        return "Not gonna do that. You need an n>0"
    rndArr = np.zeros([2, n])
    rndSeed = np.random.rand(n)
    for idx, ele in enumerate(rndSeed):
        if ele <= 0.4:
            rndArr[0, idx] = 0
            rndArr[1, idx] = 0
        elif ele <= 0.6:
            rndArr[0, idx] = 1
            rndArr[1, idx] = 0
        elif ele <= 0.9:
            rndArr[0, idx] = 0
            rndArr[1, idx] = 1
        else:
            rndArr[0, idx] = 1
            rndArr[1, idx] = 1
    return rndArr


n = 10000
t = toyrnd(n)
#print(t)

#   then calculate the respective empirical values
expt = np.transpose(np.sum(t, 1))/n
print('The expectation is: ', expt)

diff = np.transpose(t)-expt
diff = np.transpose(diff)
cov = np.dot(diff, np.transpose(diff))/n
print('The covariance matrix is: ', cov)