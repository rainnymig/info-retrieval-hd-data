import numpy as np
import matplotlib.pyplot as plt

#   the number of dimensions
p_range = range(2, 1001)
#   sample count (point count)
n = 10
#   other sample count settings
#n = 10
#n = 200

#   the function to generate n points in a p dimentional hypercube
def points_generator(n, p):
    #   initialize the list to contain the points
    points = np.random.uniform(-1.0, 1.0, (n, p))
    return points

#   the function to calculate the COSINE of the angle between two points
def angle_cos_calculator(p1, p2):
    angle = np.dot(p1, p2)/(np.linalg.norm(p1)*np.linalg.norm(p2))
    return angle

average_min_angles = []

for p in p_range:
    #   the vector to contain the maximum cosine value (which in this case means the minimum angle)
    #   of the angles of a point to all other points
    #   it is set to -1 initially
    max_angles_cos = np.zeros(n)-1
    points = points_generator(n, p)
    for idx, point in enumerate(points):
        for other_idx, other_point in enumerate(points[(idx+1):]):
            #   calculate the cosine of the angle between two points
            angle_cos = angle_cos_calculator(point, other_point)
            #   if this cosine value is greater than the value 
            #   corresponding to the two points which is currently stored in the vector
            #   then replace that value
            if angle_cos > max_angles_cos[idx]:
                max_angles_cos[idx] = angle_cos
            if angle_cos > max_angles_cos[idx+1+other_idx]:
                max_angles_cos[idx+1+other_idx] = angle_cos
    #   convert cosine to angle
    min_angles = np.arccos(max_angles_cos)
    #   calculate the average minimum
    average_min_angle = np.average(min_angles)
    average_min_angles.append(average_min_angle)
    
#   plot the result of the average minimum angle related to number of dimensions
plt.xlabel('p')
plt.ylabel('average minimum angle/rad')
plt.grid(True)
plt.title('average minimum angles of ' + str(n)+ ' samples')
plt.plot(p_range, average_min_angles)


#   Conclusions:
#       We can see in the graph of average minimum angles between points 
#       in p dimensional space that in higher dimensions, the minimum angles
#       are more close to 90 degrees or pi/2 rad. This result doesn't change 
#       with sample size . 
#       In fact, I also tried to calculate the average maximum angles in 
#       2 to 1000 dimensions and it is in high dimensional space the maximum 
#       angles also tend to be close to 90 degrees. Just like that the points 
#       in high dimensional space are equidistant, we can draw the conclusion 
#       that the angles bewtween 2 points(or vectors) are also tend to be equal. 