#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 19:00:31 2017

@author: rainnymig
"""
import numpy as np

def logistic_gradient(X_train, y_train, w):
    inner = sigma_func(np.dot(X_train.transpose(), w))-y_train
    g = np.dot(X_train, inner)
    return g

def sigma_func(t):
    z = np.exp(-np.abs(t))
    return np.where(t>=0, 1.0/(1.0+z), z/(1.0+z))

#   H = XBXT
def logistic_hessian(X_train, w):
    S = sigma_func(np.dot(X_train.T, w))
    diagB = S*(1.0-S)
    XS = X_train*np.expand_dims(diagB, axis = 0)
    H = np.dot(XS, X_train.T)
    return H

def find_w(X_train, y_train, max_it, reg=0.0,w_init=None):
    w=np.zeros((X_train.shape[0]))
    if w_init is not None:
        w=w_init
    cost=np.inf
    for i in range(max_it):
        delta_w=np.linalg.lstsq(logistic_hessian(X_train, w),logistic_gradient(X_train, y_train, w))[0]
        w-=np.array(delta_w)
        S=sigma_func(np.dot(w.T,X_train))
        oldcost=cost
        cost=-(np.dot(np.log(np.where(S<np.finfo('float32').eps,np.finfo('float32').eps,S)),y_train)
              +np.dot(np.log(np.where((1.0-S)<np.finfo('float32').eps,np.finfo('float32').eps,1.0-S)),1.0-y_train)
              -reg*np.sum(w**2))
        if cost>=oldcost:
            break
        w_star=w
        print('Iteration:', i+1, ' Current cost:', cost)
    return w_star

def find_w_2(X_train, y_train, max_it = 1):
    n_w = len(X_train)
    w = np.zeros(n_w)
    cost = np.inf
    for ite in range(max_it):
        H = logistic_hessian(X_train, w)
        g = logistic_gradient(X_train, y_train, w)
        w = w-np.linalg.lstsq(H, g)[0]
        S = sigma_func(np.dot(w.T, X_train))
        old_cost = cost
        cost = -(np.dot(np.log(np.where(S < np.finfo("float32").eps, np.finfo("float32").eps, S)), y_train))+np.dot(np.log(np.where((1.0-S) < np.finfo('float32').eps, np.finfo('float32').eps, 1.0-S)), 1.0-y_train)
        if cost >= old_cost:
            break
        print('cost in ', ite, ' iteration: ', cost)
    return w

def classify_log(w, X_test):
    y_test = []
    for tx in X_test.transpose():
        sig = np.dot(w, tx)
        if sig > 0:
            y_test.append(1)
        else:
            y_test.append(0)
    return y_test


train_data_features = np.load('X_train.npy')
test_data_features = np.load('X_test.npy')
train_lables = np.load('y_train.npy')
test_lables = np.load('y_test.npy')

n_train = len(train_data_features[0])
n_test = len(test_data_features[0])

train_plus = np.ones((1, n_train))
test_plus = np.ones((1, n_test))

train_data_features = np.vstack((train_plus, train_data_features))
test_data_features = np.vstack((test_plus, test_data_features))

w = find_w(train_data_features, train_lables, 10)
test_result = classify_log(w, test_data_features)

from sklearn.metrics import roc_auc_score as AUC
auc = AUC(test_result, test_lables)
print('AUC score after 10 iteration:',auc)