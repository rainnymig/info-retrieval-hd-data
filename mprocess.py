#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 21:01:39 2017

@author: rainnymig
"""

import numpy as np
from scipy import misc

pathPrefix = './mnist/'
path1 = 'd0/d0_0001.png'
path2 = 'd0/d0_0002.png'

img1 = misc.imread(pathPrefix+path1)
img2 = misc.imread(pathPrefix+path2)
img1 = img1/255
img2 = img2/255
diff = np.subtract(img1, img2)

#   using numpy.linalg.norm
norm1 = np.linalg.norm(diff, 'fro')
print(norm1)

#   directly apply formula 1
norm2 = np.sqrt(np.trace(np.dot(diff, np.transpose(diff))))
print(norm2)

#   euclidean norm
vtrdiff = np.ravel(diff)
norm3 = np.sqrt(np.sum(np.square(vtrdiff)))
print(norm3)