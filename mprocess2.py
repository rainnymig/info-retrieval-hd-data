#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 22:24:32 2017

@author: rainnymig
"""

import numpy as np
from scipy import misc

start = 1200
n_train = 500

pathPrefix = './mnist/'
pathDirs = [2, 6]
imgIds = range(start, start+n_train)

X_train = np.zeros([2*n_train, 784])
Y_train = np.zeros([2*n_train, ])
idx = 0

for pathDir in pathDirs:
    for imgId in imgIds:
        img = misc.imread(pathPrefix+'d'+str(pathDir)+'/d'+str(pathDir)+'_'+str(imgId)+'.png')
        img = img/255
        imgVtr = np.ravel(img)
        X_train[idx] = imgVtr
        Y_train[idx] = pathDir
        idx = idx+1

start = 1800
n_test = 10

imgIds = range(start, start+n_test)

X_test = np.zeros([2*n_test, 784])
Y_test = np.zeros([2*n_test, ])
idx = 0

k = 20
NN = np.zeros([2*n_test, k])
Y_kNN = np.zeros(2*n_test)

for pathDir in pathDirs:
    for imgId in imgIds:
        img = misc.imread(pathPrefix+'d'+str(pathDir)+'/d'+str(pathDir)+'_'+str(imgId)+'.png')
        img = img/255
        imgVtr = np.ravel(img)
        X_test[idx] = imgVtr
        Y_test[idx] = pathDir
        idx = idx+1
        
idx = 0
normList = list(range(2*n_train))

for testObj in X_test:
    for objIdx, trainObj in enumerate(X_train):
        diff = testObj-trainObj
        norm = np.sqrt(np.sum(np.square(diff)))
        normList[objIdx] = (objIdx, norm)
    normList.sort(key=lambda normList:normList[1])
    for normIdx, ele in enumerate(normList[0:20]):
        NN[idx, normIdx] = ele[0]
    idx = idx+1

for NNidx, ele in enumerate(NN):
    numl1 = 0
    numl2 = 0
    for subele in ele:
        if Y_train[int(subele)] == pathDirs[0]:
            numl1 = numl1+1
        elif Y_train[int(subele)] == pathDirs[1]:
            numl2 = numl2+1
    if numl1 >= numl2:
        Y_kNN[NNidx] = pathDirs[0]
    else:
        Y_kNN[NNidx] = pathDirs[1]
        
print(Y_kNN)