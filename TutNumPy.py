import numpy as np

print('a)')
n = np.array([1, 9, 9, 4, 0, 6, 2, 2])
print(n)

print('\nb)')
n_odd = [x for x in n if x%2 == 1]
print(n_odd, type(n_odd))

print('\nc)')
n_rev = n[::-1]
print(n_rev)

print('\nd)')
a = np.array([1, 2, 3, 4, 5])
b = a[1:4]
b[0] = 200
print(a[1])

print('\ne)')
m = np.array([[1, 4, 1, 9], [1, 4, 2, 0], [1, 4, 2, 1]])
print(m)

print('\nf)')
m_revrowel = m[:, ::-1]
print(m_revrowel)

print('\ng)')
m_revall = m[::-1, ::-1]
print(m_revall)

print('\nh)')
m_cut = np.array([[m[0, 0], m[0, -1]], [m[-1, 0], m[-1, -1]]])
print(m_cut)
