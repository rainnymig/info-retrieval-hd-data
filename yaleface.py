import glob
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt

#   import image from given path
#   return the normalized vectorized data
#   and label
#   and mean value
def import_img(path, p = 2500):
    file_list = glob.glob(path)
    N = len(file_list)
    T = np.zeros((p, N))
    T_label = []
    for idx, file_name in enumerate(file_list):
        img = np.ravel(misc.imread(file_name) / 255)
        T[:, idx] = img
        ppos = file_name.index("person")
        T_label.append(file_name[ppos+6:ppos+8])
    mu = np.average(T, 1)
    T_c = T - np.expand_dims(mu, 1)
    return [T, T_label, mu]

def centerize(T, mu):
    return T_c = T - np.expand_dims(mu, 1)

def get_sgval(T_c, k = 20, showimg = False):
    [U, Sigma, VT] = np.linalg.svd(T)
    U_k = U[:, :k]
    Sigma_k = Sigma[:k, :k]
    VT_k = VT[:k, :]
    if showimg:
        if k >= 3:
            for vec in U_k[:, :3].T:
                img = np.reshape(vec, (50, 50))
                plt.figure()
                plt.imshow(img)
                plt.show()
    return [U_k, Sigma_k, VT_k]

def fuck_it(T_c, T_label, S_c, S_label, U_k, Sigma_k, VT_k, k):
    for ki in range(k):
        #   project the training set to a lower dimension
        T_k = np.dot(Sigma_k, VT_k)



train_path = "./yaleBfaces/subset0/*.png"
[T, T_label, mu] = import_img(train_path)
T_c = centerize(T, mu)

test_path = "./yaleBfaces/subset1/*.png"
[S, S_label] = import_img(test_path)
S_c = centerize(S, mu)

[U_k, Sigma_k, VT_k] = get_sgval(T, k = 20)

print(T_label)
