#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 18:59:26 2017

@author: rainnymig
"""

import pandas as pd

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np

train = pd.read_csv('labeledTrainData.tsv', header = 0, delimiter = '\t',\
                    quoting = 3)
test = pd.read_csv('labeledTestData.tsv', header = 0, delimiter = '\t',\
                    quoting = 3)

#print("that's train: ", train.keys())
#print("let us have a closer look: ", train['sentiment'])

#   accept a raw review set
#   remove HTML tags, lower case letter, remove stop words
#   return a list, containing all processed review strings
def review_prepro(raw_reviews):
    from bs4 import BeautifulSoup
    import re
    import nltk
    from nltk.corpus import stopwords
    clean_train_reviews = []
    nltk.download('stopwords')
    stwords = stopwords.words('english')
    for item in raw_reviews['review']:
        raw_text = BeautifulSoup(item, 'lxml').get_text()
        #   use regular expressions to do a find-and-replace
        process_text = re.sub('[^a-zA-Z]', ' ', raw_text)
        process_text = process_text.lower()
        words = process_text.split()
        useful_words = [ w for w in words if not w in stwords ]
        clean_string = " ".join(useful_words)
        clean_train_reviews.append(clean_string)
    return clean_train_reviews

clean_train_reviews = review_prepro(train)
clean_test_reviews = review_prepro(test)
#print(letters_only)

vectorizer = CountVectorizer(analyzer = 'word',\
                             tokenizer = None,\
                             preprocessor = None,\
                             stop_words = None,\
                             max_features = 5000)

'''
vectorizer2 = TfidfVectorizer(max_features = 5000, \
                              ngram_range = (1, 2), \
                              sublinear_tf = True)
'''


train_data_features = vectorizer.fit_transform(clean_train_reviews)
test_data_features = vectorizer.transform(clean_test_reviews)
train_data_features = train_data_features.toarray()
test_data_features = test_data_features.toarray()

np.save('X_train.npy', train_data_features.transpose())
np.save('X_test.npy', test_data_features.transpose())

from sklearn.linear_model import LogisticRegression as LR

model = LR()
model.fit(train_data_features, train['sentiment'])
p = model.predict_proba(test_data_features)[:, 1]
output = pd.DataFrame(data = {'id': test['id'], 'sentiment': p})

from sklearn.metrics import roc_auc_score as AUC

auc = AUC(test['sentiment'].values, p)

print("And the result auc is: ", auc)
