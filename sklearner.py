#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 11:09:37 2017

@author: rainnymig
"""

import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics

digits = datasets.load_digits()

for key, value in digits.items():
    try:
        print(key, value.shape)
    except:
        print(key)
        
images_and_labels = list(zip(digits.images, digits.target))
for index, (image, label) in enumerate(images_and_labels[:4]):
    plt.subplot(2, 4, index+1)
    plt.axis('off')
    plt.imshow(image, cmap = plt.cm.gray_r, interpolation = 'nearest')
    plt.title('Training: %i' % label)
    
n_samples = len(digits.images)

data = digits.image.reshape((n_samples, -1))

classifier = svm.SVC(gamma = 0.001)
classifier.fit(data[:n_samples/2], digits.targit[:n_samples/2])

expected = digits.target[n_samples/2:]

predicted = classifier.predict(data[n_samples/2:])
