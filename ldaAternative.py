import numpy as np
import scipy.misc as scm
import matplotlib.pyplot as plt

N = 1000
digit_prefix = ['d1', 'd2', 'd3']
X = np.zeros((784, 3*N))
C = np.zeros((3*N, 3))

betas = [2, 4, 6, 8]

#   read image files and prepare data
for idx, dp in enumerate(digit_prefix):
    val_rgb = np.zeros((3,))
    val_rgb[idx] = 1.0
    for j in range(N):
        X[:, idx*N+j] = np.float64(scm.imread('mnist/'+dp+'/'+dp+'_'+'%04d.png'%(j+1)).ravel())
        C[idx*N+j] = val_rgb

#   calculate the overall mean and center the data
mu = np.mean(X, axis=1)
Xc = X-np.expand_dims(mu, axis=1)

#   calculate the means of each class
X_1 = X[:, :N]
X_2 = X[:, N:2*N]
X_3 = X[:, 2*N:3*N]
mu1 = np.mean(X_1, axis=1)
mu2 = np.mean(X_2, axis=1)
mu3 = np.mean(X_3, axis=1)

#   compute between class scatter matrix
muc = np.vstack((mu1, mu2, mu3)).T

mud = muc-np.expand_dims(mu, axis=1)
Sb = N*np.dot(mud, mud.T)

#   compute within class scatter matrix
X1c = X_1-np.expand_dims(mu1, 1)
X2c = X_2-np.expand_dims(mu2, 1)
X3c = X_3-np.expand_dims(mu3, 1)
Sw = np.dot(X1c, X1c.T)+np.dot(X2c, X2c.T)+np.dot(X3c, X3c.T)

plt.figure(figsize=(16, 12))
idx_new=((np.arange(3*N).reshape(3,N).T).reshape(N,3)).ravel()
#   project data to lower space with different betas
for idx, beta in enumerate(betas):
    #   compute Sb-beta*Sw and do eigenvalue decomposition
    #   take the 2 normalized eigenvectors corresponding to the 2 largest eigenvalues
    #   and project the data to the space spanned by them
    Sbw = Sb-beta*Sw
    W, V = np.linalg.eigh(Sbw)
    scores_lda = np.dot(V.T, Xc)
    plt.subplot(220+idx+1)
    plt.title('beta = '+str(beta))
    plt.scatter(scores_lda[-1, idx_new], scores_lda[-2, idx_new], c=C[idx_new])
    
plt.show()