from cvxopt import solvers, matrix
import numpy as np

def minsq(A, y):
    P = matrix(np.dot(A.T, A).astype('double'))
    q = matrix(-np.dot(A.T, y).astype('double'))
    x = solvers.qp(P, q)
    return np.array(x['x'])

def solvedualsvm(H, y):
    L = y.shape[0]  # training data count
    lambda_star = solvers.qp(matrix(H.astype('double')), matrix(-np.ones((L,1)).astype('double')), matrix(-np.identity(L).astype('double')), matrix(np.zeros((L,1)).astype('double')), matrix(y.reshape(1, L).astype('double')), matrix(np.zeros((1,)).astype('double')))
    return lambda_star['x']


A = np.array([[10, 40], [20, 0], [-30, 40]])
#y = np.array([50, 20, 10])+np.random.randn(3,)
y = np.array([50, 20, 10])
print('A:', A)
print('y:', y)
print('x:', minsq(A, y))
print('np.dot(pinv(A), y):', np.dot(np.linalg.pinv(A), y))


X = np.array([[-1, -2, 1, 2], [-1, -2, 1, 2]])
y = np.array([-1, -1, 1, 1])
H = np.dot(np.dot(np.dot(np.diag(y), X.T), X), np.diag(y))
lambda_star = solvedualsvm(H, y)
print('lambda_star: ', lambda_star)
