import numpy as np
from cvxopt import solvers, matrix
from scipy import misc

def solvedualsvm(H, y):
    L = y.shape[0]  # training data count
    lambda_star = solvers.qp(matrix(H.astype('double')), matrix(-np.ones((L,1)).astype('double')), matrix(-np.identity(L).astype('double')), matrix(np.zeros((L,1)).astype('double')), matrix(y.reshape(1, L).astype('double')), matrix(np.zeros((1,)).astype('double')))
    return lambda_star['x']

def simplesvm(X_train, y_train, X_test):
	H = np.dot(np.dot(np.dot(np.diag(y_train), X_train.T), X_train), np.diag(y_train))
	lambda_star = solvedualsvm(H, y_train)
	w_star = np.dot(np.dot(X_train, np.diag(lambda_star)), y_train)
	return w_star

N = 500
digit_prefix = ['d1', 'd2']
digit_label = [1, -1]
X_train = np.zeros((784, 2*N))
y_train = np.zeros((2*N,))

M = 400
X_test = np.zeros((784, 2*M))
y_test = np.zeros((2*M,))

#   read image files and prepare data
for idx, dp in enumerate(digit_prefix):
    for j in range(N):
        X_train[:, idx*N+j] = np.float64(misc.imread('mnist/'+dp+'/'+dp+'_'+'%04d.png'%(j+1)).ravel())
        y_train[idx*N+j] = digit_label[idx]


for idx, dp in enumerate(digit_prefix):
    for j in range(M):
        X_test[:, idx*M+j] = np.float64(misc.imread('mnist/'+dp+'/'+dp+'_'+'%04d.png'%(j+M+1)).ravel())
        y_test[idx*M+j] = digit_label[idx]

w_star = simplesvm(X_train, y_train, X_test)
print('w_star: ', w_star)