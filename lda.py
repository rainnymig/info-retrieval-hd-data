import numpy as np
import scipy as sp
from scipy import misc
import matplotlib.pyplot as plt

# calculate the inverse of the square root
# of a symmetric positive semidefinite matrix
def sqrtminv(A):
    A = (A+A.T)/2
    [U, Sigma, VT] = np.linalg.svd(A)
    sqwi = 1.0/np.sqrt(Sigma)
    sqmi = np.dot(U, np.dot(np.diag(sqwi), U.T))
    return sqmi

N = 1000
start = 1200

pathPrefix = './mnist/'
pathDirs = [4, 5, 6]
imgIds = range(start, start + N)

X = []    # vectorized images
C = []    # RGB labels to identify X

for pathDir in pathDirs:
    for idx in imgIds:
        X.append(misc.imread(pathPrefix + 'd' + str(pathDir) + '/d' +
                             str(pathDir) + '_' + str(idx) +
                             '.png').ravel() / 255)
        if pathDir == pathDirs[0]:
            C.append([0, 0, 1])
        elif pathDir == pathDirs[1]:
            C.append([0, 1, 0])
        elif pathDir == pathDirs[2]:
            C.append([1, 0, 0])

X = np.array(X).T

# center the data
mu = np.average(X, 1)
X_c = X-np.expand_dims(mu, 1)

[UX, SigmaX, VTX] = np.linalg.svd(X_c, full_matrices=False)

UX_2 = np.dot(np.diag(SigmaX[:2]), VTX[:2, :])

plt.scatter(UX_2[0], UX_2[1], c=C)

X_sums = np.zeros((784, len(pathDirs)))

for idx, clss in enumerate(pathDirs):
    X_c1ss = X_c[:, idx*N:(idx+1)*N]
    clss_mu = np.sum(X_c1ss, 1)/np.sqrt(N)
    X_sums[:, idx] = clss_mu

[UXs, SigmaXs, VTXs] = np.linalg.svd(X_sums, full_matrices=False)
UXs_2 = np.dot(UXs[:, :2].T, X_c)

plt.figure()
plt.scatter(UXs_2[0], UXs_2[1], c=C)

X1c = X[:, 0:N] - np.expand_dims(X_sums[:, 0], 1)
X2c = X[:, N:2*N] - np.expand_dims(X_sums[:, 1], 1)
X3c = X[:, 2*N:3*N] - np.expand_dims(X_sums[:, 2], 1)

S_w = (np.dot(X1c, X1c.T)+np.dot(X2c, X2c.T)+np.dot(X3c, X3c.T))

S_bw = np.dot(sqrtminv(S_w), X_sums)
[USbw, SigmaSbw, VTSbw] = np.linalg.svd(S_bw, full_matrices=False)
USbw_2 = np.dot(USbw.T, X_c)

plt.figure()
plt.scatter(USbw_2[0], USbw_2[1], c=C)
plt.show()
