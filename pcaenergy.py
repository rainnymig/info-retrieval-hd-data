#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 18:06:02 2017

@author: rainnymig
"""
import glob
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt

file_list = glob.glob('./VisTex_512_512/*.ppm')

ts = 64
ss = 512
pc = 167
p = ts*ts
N = int(pc*(ss/ts)*(ss/ts))
X = np.zeros((p, N))

idx = 0
for file_name in file_list:
    img = misc.imread(file_name, flatten = True)/255
    for i in range(8):
        for j in range(8):
            X[:, idx] = np.ravel(img[i*64:(i+1)*64, j*64:(j+1)*64])
            idx = idx+1

mu = np.average(X, 1) 
X_c = X-np.expand_dims(mu, 1)

[U, Sigma, VT] = np.linalg.svd(X_c, full_matrices=False)

energy = np.dot(Sigma, Sigma)

loss_frac = []

for k in range(0, p):
    Sigma_frac = Sigma[k:]
    loss_energy = np.dot(Sigma_frac, Sigma_frac)
    loss_frac.append(loss_energy/energy)

plt.plot(loss_frac)
plt.show()